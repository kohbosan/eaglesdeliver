<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 11/2/2015
 * Time: 10:56
 */

namespace EaglesDeliver;

require __DIR__ . "/vendor/autoload.php";
require_once "DBConnect.php";
require_once "Commo.php";

class Menu extends DBConnect
{

    /**
     *  This function returns a JSON string of all the menu items available.
     */
    public function GetAllMenuItems()
    {
        $db = new DBConnect();                                      //Open DB connection
        if($db){                                                    //Connection successful
            if($prep = $db->query("SELECT * FROM products")){       //Query successful
                while($result=$prep->fetch_assoc()){
                    $menu[] = $result;                              //add all items to array
                }
                Commo::ReturnMessage(0, $menu);                     //return all items with success code
            }
            else
            {
                throw new \Exception(Commo::QUERY_FAIL);        //query fail
            }
        }
        else{
            throw new \Exception(Commo::CONNECT_FAIL);          //connection unsuccessful
        }
    }

    /**
     * Gets the menu for one restaurant.
     * @param $restID int Restaurant ID
     * @throws \Exception
     */
    public function GetRestaurantMenu($restID)
    {
        $db = new DBConnect();                                      //Open DB connection
        if($db){
            $query = "SELECT * FROM products WHERE restID=".$restID;
            if($prep = $db->query($query)){                         //Query successful
                if($prep->num_rows > 0){                            //product with ID exists
                    while($result=$prep->fetch_assoc()){
                        $menu[] = $result;                          //add all items to array
                    }
                    Commo::ReturnMessage(0, $menu);                 //return all items with success code
                }
                else
                {
                    throw new \Exception(Commo::EMPTY_RESULT);  //item with ID provided not found
                }
            }
            else
            {
                throw new \Exception(Commo::QUERY_FAIL);        //query unsuccessful
            }
        }
        else{
            throw new \Exception(Commo::CONNECT_FAIL);                     //connection unsuccessful
        }
    }

    /**
     * Get the customizations for one item.
     * @param $prodID Product ID
     * @throws \Exception
     */
    public function GetItemCustoms($prodID){
        $db = new DBConnect();                                      //Open DB connection
        if($db){
            $query = "SELECT customID,description,price
                      FROM productcustoms
                      LEFT JOIN customizations
                      ON productcustoms.customID=customizations.id
                      WHERE productID=".$prodID;

            if($prep = $db->query($query)){                         //Query successful
                if($prep->num_rows > 0){                            //product with ID exists
                    while($result=$prep->fetch_assoc()){
                        $customs[] = $result;                       //add all items to array
                    }
                    Commo::ReturnMessage(0, $customs);              //return all items with success code
                }
                else
                {
                    throw new \Exception(Commo::NO_CUSTOMS);         //item with ID provided not found
                }
            }
            else
            {
                throw new \Exception(Commo::QUERY_FAIL);        //query unsuccessful
            }
        }
        else{
            throw new \Exception(Commo::CONNECT_FAIL);          //connection unsuccessful
        }
    }
}

try{
    if(isset($_GET['f'])){
        switch($_GET['f']){
            case "getitemcustoms":
                $menu = new Menu();
                $menu->GetItemCustoms($_GET['id']);
                break;
            case "getrest":
                $menu = new Menu();
                $menu->GetRestaurantMenu($_GET['id']);
                break;
            case "getall":
                $menu = new Menu();
                $menu->GetAllMenuItems();
                break;
            default:
                throw new \Exception(Commo::UNKNOWN_API);
                break;
        }
    }
}
catch(\Exception $e){
    Commo::ReturnMessage(1, $e->getMessage());
}
?>