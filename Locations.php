<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/4/2015
 * Time: 14:11
 */

namespace EaglesDeliver;

require_once "DBConnect.php";
require_once "Commo.php";

class Locations
{

    public function __construct(){

    }

    public function getAllDestinations(){
        $db = new DBConnect();
        if($db){                                                            //DB connect success
            $query = "SELECT * FROM buildings ORDER BY region, namelong";
            if($prep = $db->query($query)){                                 //query success
                while($result=$prep->fetch_assoc()){
                    $destinations[] = $result;                              //add all items to array
                }
                Commo::ReturnMessage(0, $destinations);
            }
            else
            {
                Commo::ReturnMessage(1, Commo::QUERY_FAIL);                 //query fail
            }
        }
        else
        {
            Commo::ReturnMessage(1, Commo::CONNECT_FAIL);                   //DB connect fail
        }
    }
}

if(isset($_GET['f']))
{
    switch ($_GET['f'])
    {
        case 'getAllDest':
            $loc = new Locations();
            $loc->getAllDestinations();
    }
}
else
{
    Commo::ReturnMessage(1, Commo::UNKNOWN_API);
}