<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/4/2015
 * Time: 20:39
 */
const BOT_ID = "83ebd478d5cd504a55de9a04de";
const GROUP_ID = "16212429";
const URL = "https://api.groupme.com/v3/bots/post";

function sendMessage($text){
    $post = [
        "bot_id" => BOT_ID,
        "text" => $text
    ];

    $curl = curl_init(URL);
    curl_setopt($curl, CURLOPT_POST, 1);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
    curl_exec($curl);
}