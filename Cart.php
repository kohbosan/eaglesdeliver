<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/3/2015
 * Time: 15:32
 */
namespace EaglesDeliver;

require __DIR__ . "/vendor/autoload.php";
require_once "DBConnect.php";
require_once "Commo.php";
require_once "Auth.php";

class Cart
{
    public function __construct()
    {
        Auth::sec_session_start();
        if (!isset($_SESSION['items'])) {
            $_SESSION['items'] = array();
        }
    }

    function addItem()
    {
        $query = 'SELECT ID, description, price FROM products WHERE products.ID='.$_POST['item'];
        $result = $this->sendQuery($query);
        if ($result->num_rows == 1) {                     //product exists in DB
            $result = $result->fetch_assoc();
            array_push($_SESSION['items'], array(
                "id" => $result['ID'],
                "description" => $result['description'],
                "price" => $result['price'],
                "customs" => (isset($_POST['cust']) ? $this->customsToArray($_POST['cust']) : '')));
            $this->getCart();
        } else {
            throw new \Exception(Commo::EMPTY_RESULT); //product not in DB
        }
    }

    function sendQuery($query)
    {
        $db = new DBConnect();
        if ($db) {
            $prep = $db->query($query);
            if ($prep) {
                if ($prep->num_rows > 0) {
                    return $prep;
                }
                throw new \Exception(Commo::EMPTY_RESULT);
            } else {
                throw new \Exception(Commo::QUERY_FAIL);
            }
        } else {
            throw new \Exception(Commo::CONNECT_FAIL);         //db connect failed
        }
    }

    function customsToArray($customs)
    {
        $cust_array = explode(",", $customs);
        $cust_array_size = count($cust_array);
        $query = "SELECT description, price FROM customizations WHERE ";
        for ($i = 0; $i < $cust_array_size - 1; $i++) {
            $query .= "id=" . $cust_array[$i] . " OR ";
        }
        $query .= "id=" . $cust_array[$cust_array_size - 1];
        $result = $this->sendQuery($query);
        if ($result) {
            $cust_with_db = array();
            while ($row = $result->fetch_assoc()) {
                $cust_with_db[] = $row;
            }
            return $cust_with_db;
        } else {
            throw new \Exception(Commo::QUERY_FAIL);     //query failed
        }

    }

    function removeItem()
    {
        unset($_SESSION['items'][$_POST['del']]);
        $this->getCart();
    }

    function emptyCart()
    {
        $_SESSION['items'] = array();
        $this->getCart();
    }

    function getCart()
    {
        Commo::ReturnMessage(0, $_SESSION['items']);
    }
}

try {
    if (isset($_GET['f'])) {
        $cart = new Cart();

        switch ($_GET['f']) {
            case "add":
                $cart->addItem();
                break;
            case "del":
                $cart->removeItem();
                break;
            case "get":
                $cart->getCart();
                break;
            case "empty":
                $cart->emptyCart();
                break;
        }
    } else {
        throw new \Exception(Commo::UNKNOWN_API);
    }
} catch (\Exception $e) {
    Commo::ReturnMessage(1, $e->getMessage());
}
?>