<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/4/2015
 * Time: 19:33
 */

namespace EaglesDeliver;


class Commo
{
    const CONNECT_FAIL = "DB connection failed.";
    const QUERY_FAIL = "DB query failed";
    const EMPTY_RESULT = "ID provided does not match a database item.";
    const NO_CUSTOMS = "Item with ID provided does not have any registered customizations.";
    const UNKNOWN_API = "Unknown API call";
    const ITEM_NOT_IN_CART = "Unable to process request. That item wasn't found in the cart";
    const WRONG_CREDENTIALS = "A user with those credentials could not be found in the database.";
    const POST_VAR_ERROR = "Incorrect post parameters.";
    const PASSWORD_HASH_NOT_128 = "Hashed password is not 128 characters in length.";
    const USER_ALREADY_EXISTS = "A user with that username already exists";
    const EMAIL_ALREADY_EXISTS = "A user with that e-mail address already exists.";

    /**
     * Formats outgoing data with status code.
     * @param $code 0=success,1=fail
     * @param $msg String error message
     */
    public static function ReturnMessage($code, $msg)
    {
        if($code)
        {
            //error occurred
            echo(json_encode([ "status" => $code, "error" => $msg]));
        }
        else
        {
            echo(json_encode([ "status" => $code, "data" => $msg ]));
        }
    }
}