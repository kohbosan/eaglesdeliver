# EaglesDeliver - API Reference
[![Build Status](https://magnum.travis-ci.com/kohbo/EaglesDeliver.svg?token=2Ss2uR6UjtNppL2vVRTL&branch=master)](https://magnum.travis-ci.com/kohbo/EaglesDeliver) 

### Menu Handlers

**Get menu for one restaurant**
```
GET /menu/get/{restaurant_id}
```
**Get all menu items**
```
GET /menu/get/all
```
**Get all restaurant information**
```
GET /menu/restaurants
```
**Get customizations for one item**
```
GET /item/get/{item_id}
```

###Cart Handlers

**Add item**
```
POST /cart/add
```
Parameters

item : id of item to add to cart

cust : comma separated list of customization id's to include with this item

**Remove item**
```
POST /cart/del
```
Parameters
del : cart_id of item to remove from cart

**Empty Cart**
```
GET /cart/empty
```

**Get cart contents**
```
GET /cart/get
```

###Location Handlers

**Get all registered destinations**
```
GET /locations/all
```
