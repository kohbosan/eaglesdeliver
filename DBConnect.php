<?php
/**
 * Created by PhpStorm.
 * User: Juan
 * Date: 11/2/2015
 * Time: 11:25
 */

namespace EaglesDeliver;

use mysqli;

require_once "DBConstants.php";

class DBConnect extends mysqli implements DBConstants
{
    public function __construct()
    {
        parent::__construct(DBConstants::DB_HOST, DBConstants::DB_USER, DBConstants::DB_PASS, DBConstants::DB_NAME);
    }
}
?>