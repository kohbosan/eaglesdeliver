<?php
/**
 * Created by PhpStorm.
 * User: kohbo
 * Date: 11/16/2015
 * Time: 20:04
 */

namespace EaglesDeliver;

require_once "DBConnect.php";
require_once "Commo.php";

class Auth{
    public static function sec_session_start(){
        try{
            $session_name = 'sec_session';
            $secure = TRUE;
            $httponly = TRUE;
            $cookie_params = session_get_cookie_params();
            session_set_cookie_params(
                $cookie_params["lifetime"],
                $cookie_params["path"],
                $cookie_params["domain"],
                $secure,
                $httponly
                );

            session_name($session_name);
            session_start();
            session_regenerate_id(true);
        }
        catch(\Exception $e){
            Commo::ReturnMessage(1, $e->getMessage());
        }
    }

    public function login(){
        if(!isset($_POST['uname'], $_POST['p'])){
            throw new \Exception(Commo::POST_VAR_ERROR);
        }
        $username = $_POST["uname"];
        $password = $_POST['p'];
        $db = new DBConnect();
        $query = "SELECT password, salt FROM users WHERE username = ? LIMIT 1";
        $prep = $db->prepare($query);
        $prep->bind_param("s", $username);
        if (!$prep->execute()) {
            throw new \Exception(Commo::QUERY_FAIL);
        }
        $prep->store_result();
        $prep->bind_result($db_pass, $db_salt);
        $prep->fetch();

        if ($prep->num_rows != 1) {
            //username not found
            throw new \Exception(Commo::WRONG_CREDENTIALS);
        }
        //get password hash
        $password = hash("sha512", $password . $db_salt);
        if ($password != $db_pass){
            //password doesn't match
            throw new \Exception(Commo::WRONG_CREDENTIALS);
        }

        $user_browser = $_SERVER['HTTP_USER_AGENT'];
        $username = preg_replace("/[^a-zA-Z0-9_]+/",
            "",
            $username);

        $_SESSION['username'] = $username;
        $_SESSION['login_string'] = hash('sha512', $password . $user_browser);
        Commo::ReturnMessage(0, "Logged in");
    }

    public static function check_logged_in(){
        if(isset($_SESSION['username'], $_SESSION['login_string'])){
            $username = $_SESSION['username'];
            $user_browser = $_SERVER['HTTP_USER_AGENT'];
            $login_string = $_SESSION['login_string'];

            $db = new DBConnect();
            $query = "SELECT password FROM users WHERE username = ? LIMIT 1";
            $prep = $db -> prepare($query);
            $prep->bind_param("s", $username);
            if(!$prep->execute()) {
                //not logged in - unable to query DB
                return false;
            }
            $prep->store_result();
            if($prep->num_rows == 1){
                $prep->bind_result($db_pass);
                $prep->fetch();
                $login_check = hash('sha512', $db_pass . $user_browser);

                if($login_check == $login_string){
                    //logged in
                    return true;
                }
            } else {
                //not logged in - username not found in db
                return false;
            }
        } else {
            //not logged in - session vars not set
            return false;
        }
    }

    /**
     *
     */
    public function logout(){
        self::sec_session_start();
        $_SESSION = array();
        $params = session_get_cookie_params();
        setcookie(
            session_name(),
            '',
            time() - 42000,
            $params['path'],
            $params['domain'],
            $params['secure'],
            $params['httponly']);

        session_destroy();
        header("Location: /");
    }
}

try{
    if(isset($_GET['f'])){
        $auth = new Auth();
        switch($_GET['f']){
            case 'login':
                $auth->login();
                break;
            case 'logout':
                $auth->logout();
                break;
            default:
                Commo::ReturnMessage(1, Commo::UNKNOWN_API);
                break;
        }
    }
}
catch (\Exception $e){
    Commo::ReturnMessage(1, $e->getMessage());
}
